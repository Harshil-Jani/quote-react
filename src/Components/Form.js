import React, { useState } from "react";
import '../Styles/Form.css'

export default function Form(){
    const [quote, setQuote] = useState("Quote goes here");
    const [ref, setRef] = useState("Reference");
    const [source, setSource] = useState("");
    const [valid,setValid] = useState(true);

    const validation = new RegExp('.*[a-zA-Z]+.*');
    if(!validation.test(quote)){
        setQuote("Invalid Quote : Cannot Be Empty");
        setValid(false);
    }
    function handleChangeQuote(event){
        setValid(true);
        setQuote(event.target.value);
    }
    function handleRefName(event){
        setRef(event.target.value);
    }
    function handleSource(event){
        setSource(event.target.value);
    }
    
    return (
        <>
        <div className="userinput-component">
        <label>Quotation</label>
        <textarea onChange={handleChangeQuote}>Input your Quote here.</textarea>
        <label>Ref. Name</label>
        <input id="Ref" onChange={handleRefName} placeholder="Eg : The Programmer's Oath"></input>
        <label>Page No. / URL / ISBN</label>
        <input id="source" onChange={handleSource} placeholder="Eg : By Robert C. Martin (Uncle Bob)"></input>
        </div>
        <div className="preview-component">
            <div className="quote-display">
                <div className="line"></div>
                {valid && <div className="quote-text"><b className="quotation-marks">"</b>{quote}<b className="quotation-marks">"</b></div>}
                {!valid && <div className="quote-text">Invalid Quote</div>}
            </div>
            <div className="Reference"><b>~ </b>{ref} {source}</div>
        </div>
        </>
    );
}